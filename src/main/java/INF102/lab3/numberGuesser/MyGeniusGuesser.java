package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

	@Override
    public int findNumber(RandomNumber number) {
        int upper = number.getUpperbound();
        int lower = number.getLowerbound();
        return findNumberRecursive(number, upper, lower);
    }

    private int findNumberRecursive(RandomNumber number, int upper, int lower){
        int guess = lower + ((upper - lower) / 2);
        int result = number.guess(guess);

        if (result == -1){
            lower = guess;
        } else if (result == 1){
            upper = guess;
        } else if (result == 0){
            return guess;
        }
        return findNumberRecursive(number, upper, lower);
    }

}
