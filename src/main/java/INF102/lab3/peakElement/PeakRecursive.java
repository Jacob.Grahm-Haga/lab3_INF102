package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        return peakElementRecursive(numbers, 0); //Kickstart
    }

    private int peakElementRecursive(List<Integer> numbers, int index){
        int current = numbers.get(index);
        int next = 0;
        int prev = 0;
        if (index != 0){
            prev = numbers.get(index - 1);
        }
        if (index != numbers.size() - 1){
            next = numbers.get(index + 1);
        }

        if ((prev <= current)&&(current >= next)){
            return current;
        } else if (index != numbers.size()) {
            return peakElementRecursive(numbers, index + 1);
        } else {
            return 0;
        }
    }

}
