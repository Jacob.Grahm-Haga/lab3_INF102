package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        return recursiveSum(list, 0); //Kickstart
    }

    private long recursiveSum(List<Long> list, int index){
        if (index == list.size()){ // End recursion on list end
            return 0;
        }
        return list.get(index) + recursiveSum(list, index + 1);
    }
}
